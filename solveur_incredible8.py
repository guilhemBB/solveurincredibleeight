#!/usr/bin/python
#Guilhèm BLANCHARD
#Le 01/04/2020
#Publié sous WTFPL2

solution = ['x','a','a','a','a','a','a','a','a']
estIdentiquePrecedent = ['x','x',False,False,False,False,False,False,False]
occurenceDeABCD=[0,0,0,0]

def compteOccurenceDeABCD():
    for i in range(0,4):
        occurenceDeABCD[i]=0

    for i in range(1,9):
        if(solution[i]=='a'):
            occurenceDeABCD[0]+=1
        elif(solution[i]=='b'):
            occurenceDeABCD[1]+=1
        elif(solution[i]=='c'):
            occurenceDeABCD[2]+=1
        elif(solution[i]=='d'):
            occurenceDeABCD[3]+=1

##Pour Q1, on compte le nombre de réponse C.
def compteC():
    compteOccurenceDeABCD()
    return occurenceDeABCD[2]

##Pour Q2, on cherche combien de fois la réponse la moins courante du quizz apparait
def repMoinsCouranteOccEst():
    compteOccurenceDeABCD()
    leMin=min(occurenceDeABCD)
    return leMin

#Pour Q2, on cherche qualle est la reponse la mois courrante
def repMoinsCouranteEst():
    occ=repMoinsCouranteOccEst()
    for i in range(0,4):
        if(occurenceDeABCD[i]==occ):
            res=i
    if(res==0):
        return 'a'
    elif(res==1):
        return 'b'
    elif(res==2):
        return 'c'
    elif(res==3):
        return 'd'

##Pour Q3, on cherche combien de fois la réponse la plus courante du quizz apparait
def repPlusCouranteOccEst():
    compteOccurenceDeABCD()
    leMax=max(occurenceDeABCD)
    return leMax

#Pour Q3, on cherche qualle est la reponse la mois courrante
def repPlusCouranteEst():
    occ=repPlusCouranteOccEst()
    for i in range(0,4):
        if(occurenceDeABCD[i]==occ):
            res=i
    if(res==0):
        return 'a'
    elif(res==1):
        return 'b'
    elif(res==2):
        return 'c'
    elif(res==3):
        return 'd'


###Pour Q7, on cherche quel sont les questions consecutives avec la meme reponse et s'y il n'y a qu'une seule paire
def checkConsec():
    #Raz de estIdentiquePrecedent
    for i in range(2,9):
        estIdentiquePrecedent[i]=False
    for i in range(1, 8):
        if(solution[i]==solution[i+1]):
            estIdentiquePrecedent[i+1]=True

def unSeulConsec():
    tot=0
    for i in range(2,9):
        if(estIdentiquePrecedent[i]):
            tot+=1

    return tot

#Pour Q8, on cheche si la question en parametre est la seule a avoir la meme reponse que Q8

def verifPourQ8(QARepId):
    listeQuestionACheck=[1,2,3,4,5,6,7]
    listeQuestionACheck.remove(QARepId)
    tousLesAutreDiff=True
    for i in listeQuestionACheck:
        if(solution[i]==solution[8]):
            tousLesAutreDiff=False
    return (tousLesAutreDiff and solution[QARepId]==solution[8])


def assertQ1():
    rep=solution[1]
    compteOccurenceDeABCD()
    if(rep=='a'):
        return occurenceDeABCD[2]==1
    if(rep=='b'):
        return occurenceDeABCD[2]==2
    if(rep=='c'):
        return occurenceDeABCD[2]==3
    if(rep=='d'):
        return occurenceDeABCD[2]==4

def assertQ2():
    rep=solution[2]
    compteOccurenceDeABCD()
    if(rep=='a'):
        return repMoinsCouranteEst()=='d'
    if(rep=='b'):
        return repMoinsCouranteEst()=='b'
    if(rep=='c'):
        return repMoinsCouranteEst()=='c'
    if(rep=='d'):
        return repMoinsCouranteEst()=='a'

def assertQ3():
    rep=solution[3]
    compteOccurenceDeABCD()
    if(rep=='a'):
        return repPlusCouranteEst()=='d'
    if(rep=='b'):
        return repPlusCouranteEst()=='c'
    if(rep=='c'):
        return repPlusCouranteEst()=='b'
    if(rep=='d'):
        return repPlusCouranteEst()=='a'

def assertQ4():
    rep=solution[4]
    compteOccurenceDeABCD()
    if(rep=='a'):
        return occurenceDeABCD[0]==4
    if(rep=='b'):
        return occurenceDeABCD[0]==1
    if(rep=='c'):
        return occurenceDeABCD[1]==0
    if(rep=='d'):
        return occurenceDeABCD[0]==occurenceDeABCD[2]

def assertQ5():
    rep=solution[5]
    if(rep=='a'):
        return (solution[1]!='a' and solution[2]!='a' and solution[3]!='a' and solution[4]!='a' and solution[5]=='a')
    if(rep=='b'):
        return (solution[1]!='a' and solution[2]!='a' and solution[3]!='a' and solution[4]!='a' and solution[5]!='a' and solution[6]=='a')
    if(rep=='c'):
        return (solution[1]!='a' and solution[2]!='a' and solution[3]!='a' and solution[4]!='a' and solution[5]!='a' and solution[6]!='a' and solution[7]=='a')
    if(rep=='d'):
        return solution[1]=='a'

def assertQ6():
    rep=solution[6]
    if(rep=='a'):
        return repPlusCouranteOccEst()==3
    if(rep=='b'):
        return repPlusCouranteOccEst()==4
    if(rep=='c'):
        return repPlusCouranteOccEst()==5
    if(rep=='d'):
        return repPlusCouranteOccEst()==6

def assertQ7():
    rep=solution[7]
    checkConsec()
    if(rep=='a'):
        return (unSeulConsec()==1 and estIdentiquePrecedent[3])
    if(rep=='b'):
        return (unSeulConsec()==1 and estIdentiquePrecedent[8])
    if(rep=='c'):
        return (unSeulConsec()==1 and estIdentiquePrecedent[2])
    if(rep=='d'):
        return (unSeulConsec()==1 and estIdentiquePrecedent[5])

def assertQ8():
    rep=solution[8]
    if(rep=='a'):
        return verifPourQ8(4)
    if(rep=='b'):
        return verifPourQ8(2)
    if(rep=='c'):
        return verifPourQ8(3)
    if(rep=='d'):
        return verifPourQ8(7)

def assertAll():
    return(assertQ1() and assertQ2() and assertQ3() and assertQ4() and assertQ5() and assertQ6() and assertQ7() and assertQ8())

def incrementeColl(num):
    if(solution[num]=='a'):
        solution[num]='b'
    elif(solution[num]=='b'):
        solution[num]='c'
    elif(solution[num]=='c'):
        solution[num]='d'
    elif(solution[num]=='d'):
        solution[num]='a'
        incrementeColl(num-1)
    elif(solution[num]=='x'):
        solution[num]='X'

def incrementeSolution():
    incrementeColl(8)

print("Debut de la sequence ")
for i in range(1,65537):
    if(assertAll()):
        print("la solution ")
        for i in range(1,9):
            print(i," ", solution[i])
        print("est valide")
    incrementeSolution()
